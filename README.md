# SQL - Pratique du SQL

Importez le fichier SQL associé à ce dépot : [SQL file](./jbf.sql)

Observez la base de données puis réalisez les requêtes SQL.

## Analyse de la base de données

![MPD](./mpd.png)

Avant de pouvoir réaliser les requêtes , prenons un temps pour comprendre l'application et la structure de cette base de données.
Cette base de données est liée au des produits alimentaires industriels et a pour but de recenser la présence d'allergènes.

Un **product** consiste en un produit industriel conçu par notre enseigne.
Il est composé d'**ingrédients**.
Les ingrédients peuvent contenir des allergènes. Mais le produit également. Par exemple, lors de la confection du produit, des traces peuvent

Un **product** peut également respecter des labels. Un label dispose d'un nom et d'un icone. Initialement destiné aux labels officiels tels que AB ou encore Label Rouge

Un **product** peut également respecter des critères. Il s'agit plus ici de filtres rapide tels que 'Végane' ou encore 'Hallal'.

## Exercice 1

Retournez le nom de l'ensemble des produits présents au sein de la base de données.

## Exercice 2

Retournez en une requête le nombre de produits présents dans la base de données

# Exercice 3

Retournez le nom des produits qui possèdent le critère **Végane**

# Exercice 4

Retournez l'ensemble des produits qui est à base de l'ingrédient **pain ciabatta**.

# Exercice 5

Retournez le nom des produits accompagnés par le nombre d'ingrédients le constituant

# Exercice 6

Produisez la requête SQL permettant de retourner le nom du produit ayant le plus d'allergènes ? 

# Exercice 7

L'administrateur a prévu des critères qui ne sont affectés à aucun produit. Afin de garantir une BDD pertinente, il serait pertinent de les retirer. Veuillez créer la requête SQL permettant de les lister.

# Exercice 8

Une équipe de développeurs souhaite ajouter la fonctionnalité permettant de créer un label.

Veuillez leur produire une requête permettant de créer un label nommé 'Label Bleu' et ayant pour url **/allergen/labels/label-bleu.png**.

Les dates de création et de mise à jour doivent être initialiseés à la date d'execution de la requête.

# Exercice 9

Retournez l'ensemble des noms des ingrédients non utilisés au sein des produits ou des tables pivots.
Ordonnés alphabétiquement et sans redondance.

# Exercice 10

L'objectif de cette base de données est de pouvoir voir la présence ou l'absence d'allergènes.

Retournez le nom des produits qui ne contiennent pas de lait (milk)

Attention, il est possible d'associer des allergènes aux produits mais aussi aux ingrédients. Votre requête doit le prendre en compte.
