-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 12 avr. 2021 à 12:04
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `justbe-food`
--

-- --------------------------------------------------------

--
-- Structure de la table `allergens`
--

DROP TABLE IF EXISTS `allergens`;
CREATE TABLE IF NOT EXISTS `allergens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_major` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `allergens`
--

INSERT INTO `allergens` (`id`, `name`, `description`, `picture`, `is_major`, `created_at`, `updated_at`) VALUES
(1, 'egg', 'description egg', 'allergen/allergen/egg.svg', 1, NULL, NULL),
(2, 'shrimp', 'description shrimp', 'allergen/allergen/shrimp.svg', 1, NULL, NULL),
(3, 'surimi', 'description surimi', 'allergen/allergen/surimi.svg', 1, NULL, NULL),
(4, 'mustard', 'description mustard', 'allergen/allergen/mustard.svg', 1, NULL, NULL),
(5, 'arachid', 'description arachid', 'allergen/allergen/arachid.svg', 1, NULL, NULL),
(6, 'mollusk', 'description mollusk', 'allergen/allergen/mollusk.svg', 1, NULL, NULL),
(7, 'sulfate', 'description sulfate', 'allergen/allergen/sulfate.svg', 1, NULL, NULL),
(8, 'celeri', 'description celeri', 'allergen/allergen/celeri.svg', 1, NULL, NULL),
(9, 'soja', 'description soja', 'allergen/allergen/soja.svg', 1, NULL, NULL),
(10, 'lupin', 'description lupin', 'allergen/allergen/lupin.svg', 1, NULL, NULL),
(11, 'glutten', 'description glutten', 'allergen/allergen/cereal.svg', 1, NULL, NULL),
(12, 'nuts', 'description nuts', 'allergen/allergen/nuts.svg', 1, NULL, NULL),
(13, 'milk', 'description milk', 'allergen/allergen/milk.svg', 1, NULL, NULL),
(14, 'fish', 'description fish', 'allergen/allergen/fish.svg', 1, NULL, NULL),
(15, 'sesame', 'description sesame', 'allergen/allergen/sesame.svg', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `criterias`
--

DROP TABLE IF EXISTS `criterias`;
CREATE TABLE IF NOT EXISTS `criterias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `criterias`
--

INSERT INTO `criterias` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Diététique', '2020-07-13 12:47:55', '2020-07-13 12:47:55'),
(2, 'Végane', '2020-07-13 12:48:02', '2020-07-13 12:48:02'),
(3, 'Végétarien', '2020-07-13 12:48:11', '2020-07-13 12:48:11'),
(4, 'Gourmand', '2020-07-13 12:48:30', '2020-07-13 12:48:30'),
(5, 'Boisson chaude', '2020-07-13 12:48:55', '2020-07-13 12:48:55');

-- --------------------------------------------------------

--
-- Structure de la table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'cheddar fondu', '2020-07-14 07:27:18', '2020-07-14 07:27:18'),
(2, 'Fourme d\'Ambert', '2020-07-14 09:34:30', '2020-07-14 09:34:30'),
(3, 'poulet', '2020-07-14 09:57:35', '2020-07-14 09:57:35'),
(4, 'poisson pané', '2020-07-14 10:29:11', '2020-07-14 10:33:08'),
(7, 'steak haché de boeuf', '2020-07-15 08:28:02', '2020-07-15 10:35:31'),
(9, 'oignons', '2020-07-15 12:05:32', '2020-07-15 12:05:32'),
(10, 'salade', '2020-07-15 12:05:53', '2020-07-15 12:05:53'),
(11, 'cornichons', '2020-07-15 12:06:08', '2020-07-15 12:06:08'),
(12, 'petit pain rond', '2020-07-15 12:07:32', '2020-07-15 12:07:32'),
(13, 'pain ciabatta', '2020-07-15 12:09:08', '2020-07-15 12:09:08'),
(14, 'emmental', '2020-07-15 12:09:22', '2020-07-15 12:09:22'),
(15, 'tomates', '2020-07-15 12:09:40', '2020-07-15 12:09:40'),
(16, 'sauce ketchup', '2020-07-15 12:09:50', '2020-07-15 12:09:50'),
(17, 'pain spécial aux éclats de bacon', '2020-07-15 12:11:00', '2020-07-15 12:11:00'),
(18, 'bacon', '2020-07-15 12:11:21', '2020-07-15 12:11:21'),
(19, 'fromage fondu', '2020-07-15 12:11:46', '2020-07-15 12:11:46'),
(20, 'capres', '2020-07-15 12:12:33', '2020-07-15 12:12:33'),
(21, 'moutarde', '2020-07-15 12:13:25', '2020-07-15 12:13:25'),
(22, 'Bâtonnets de pommes de terre frites', '2020-07-15 15:11:39', '2020-07-15 15:11:39'),
(23, 'Lait', '2020-07-15 15:37:40', '2020-07-15 15:37:40'),
(24, 'Boisson à base de jus d\'oranges concentré', '2020-07-15 15:41:58', '2020-07-15 15:41:58'),
(25, 'Jus de pomme', '2020-07-15 15:45:03', '2020-07-15 15:45:03'),
(26, 'fruits', '2020-07-15 17:28:05', '2020-07-15 17:28:05');

-- --------------------------------------------------------

--
-- Structure de la table `ingredient_has_allergen`
--

DROP TABLE IF EXISTS `ingredient_has_allergen`;
CREATE TABLE IF NOT EXISTS `ingredient_has_allergen` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) UNSIGNED NOT NULL,
  `allergen_id` bigint(20) UNSIGNED NOT NULL,
  `is_trace` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_ingredient_a_allergens_a_ingredient_id_foreign` (`ingredient_id`),
  KEY `a_ingredient_a_allergens_allergen_id_foreign` (`allergen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredient_has_allergen`
--

INSERT INTO `ingredient_has_allergen` (`id`, `ingredient_id`, `allergen_id`, `is_trace`, `created_at`, `updated_at`) VALUES
(1, 4, 14, 0, NULL, NULL),
(2, 1, 13, 0, NULL, NULL),
(3, 2, 13, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `label`
--

DROP TABLE IF EXISTS `label`;
CREATE TABLE IF NOT EXISTS `label` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `label`
--

INSERT INTO `label` (`id`, `name`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'Label AB (Agriculture Biologique)', 'allergen/labels//1594833567.png', '2020-07-15 17:19:27', '2020-07-15 17:25:29'),
(2, 'Label rouge', '/allergen/labels/label-rouge.png', '2021-04-12 10:24:49', '2021-04-12 10:24:49');

--
-- Déclencheurs `label`
--
DROP TRIGGER IF EXISTS `tr_remove_non_referenced_label_images_on_delete`;
DELIMITER $$
CREATE TRIGGER `tr_remove_non_referenced_label_images_on_delete` AFTER DELETE ON `label` FOR EACH ROW BEGIN
                IF OLD.picture THEN
                    INSERT INTO to_remove_from_server(path) VALUES (OLD.picture);
                END IF;
            END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `tr_remove_non_referenced_label_images_on_updte`;
DELIMITER $$
CREATE TRIGGER `tr_remove_non_referenced_label_images_on_updte` AFTER UPDATE ON `label` FOR EACH ROW BEGIN
                IF NEW.picture != OLD.picture THEN
                    INSERT INTO to_remove_from_server(path) VALUES (OLD.picture);
                END IF;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nutritional_intake` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `nutritional_intake`, `picture`, `created_at`, `updated_at`) VALUES
(2, 'LE BIG MAC™', 'Le seul, l\'unique\r\n\r\nSes deux steaks hachés, son cheddar fondu, ses oignons, ses cornichons, son lit de salade et sa sauce inimitable, font du Big Mac un burger culte et indémodable.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	972kJ 	2106kJ 	25%\r\n232kcal 	503kcal 	25%\r\nMatières grasses 	12g 	25g 	36%\r\ndont acides gras saturés 	4.5g 	9.7g 	49%\r\nGlucides 	19g 	42g 	16%\r\ndont sucres 	3.9g 	8.5g 	9%\r\nFibres 	1.4g 	3.1g 	\r\nProtéines 	12g 	26g 	52%\r\nSel 	1g 	2.2g 	37%', 'allergen/product//1594656593.png', '2020-07-13 16:09:53', '2020-07-13 16:09:53'),
(3, 'LE McCHICKEN', 'L\'incontournable sandwich au poulet croustillant\r\n\r\nNotre spécialité panée au poulet, sa salade croquante et sa sauce légèrement citronnée font du McChicken un succès incontesté et surtout incontestable depuis 1980.\r\n\r\nPain spécial, spécialité panée au poulet, salade, sauce.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	955kJ 	1860kJ 	22%\r\n228kcal 	444kcal 	22%\r\nMatières grasses 	10g 	19g 	28%\r\ndont acides gras saturés 	1.2g 	2.3g 	11%\r\nGlucides 	23g 	45g 	17%\r\ndont sucres 	3.3g 	6.4g 	7%\r\nFibres 	1.4g 	2.7g 	\r\nProtéines 	11g 	21g 	42%\r\nSel 	1g 	1.9g 	32%', 'allergen/product//1594657727.png', '2020-07-13 16:28:48', '2020-07-13 16:28:48'),
(4, 'LE 280™ ORIGINAL', 'Retrouvez la douceur de la recette du 280™ Original\r\n\r\nUn pain ciabatta cuit sur pierre, un steak haché 100% pur bœuf *, de l\'emmental et du cheddar fondus, des oignons frais, deux rondelles de tomates, du ketchup et une sauce légèrement citronnée.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	999kJ 	3255kJ 	39%\r\n239kcal 	778kcal 	39%\r\nMatières grasses 	13g 	42g 	60%\r\ndont acides gras saturés 	5.8g 	19g 	95%\r\nGlucides 	17g 	54g 	21%\r\ndont sucres 	1g 	4g 	4%\r\nFibres 	1.2g 	4g 	\r\nProtéines 	14g 	44g 	88%\r\nSel 	1.1g 	3.5g 	58%', 'allergen/product//1594716453.png', '2020-07-14 08:47:34', '2020-07-14 08:47:34'),
(5, 'LE BIG TASTY™', 'L\'irresistible Big Tasty™ est de retour\r\n\r\nRetrouvez sa recette unique : un steak haché 100% pur boeuf*, trois tranches d\'emmental fondu, deux rondelles de tomate, de la salade, des oignons frais et une délicieuse sauce au goût grillé.\r\n\r\nDurée limitée', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1015kJ 	3457kJ 	41%\r\n243kcal 	828kcal 	41%\r\nMatières grasses 	15g 	50g 	71%\r\ndont acides gras saturés 	5.9g 	20g 	100%\r\nGlucides 	15g 	50g 	19%\r\ndont sucres 	3g 	10g 	11%\r\nFibres 	1g 	3.3g 	\r\nProtéines 	13g 	44g 	88%\r\nSel 	1.1g 	3.7g 	61%', 'allergen/product//1594717098.png', '2020-07-14 08:58:18', '2020-07-14 08:58:18'),
(6, 'LE CHICKEN BIG™ TASTY', 'La version poulet de l\'irresistible Big Tasty™\r\n\r\nSuccombez à la gourmandise du Chicken Big Tasty™: du poulet croustillant*, trois tranches d\'emmental fondu, deux rondelles de tomate, des oignons frais, de la salade et une délicieuse sauce au goût grillé.\r\n\r\nDurée limitée', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1012kJ 	3501kJ 	42%\r\n242kcal 	838kcal 	42%\r\nMatières grasses 	13g 	48g 	68%\r\ndont acides gras saturés 	3.8g 	14g 	69%\r\nGlucides 	20g 	66g 	25%\r\ndont sucres 	3.1g 	10g 	12%\r\nFibres 	1.3g 	4.2g 	\r\nProtéines 	9.5g 	34g 	69%\r\nSel 	1.3g 	4.5g 	76%', 'allergen/product//1594717294.png', '2020-07-14 09:01:34', '2020-07-14 09:01:34'),
(7, 'LE CBO™', 'Pain spécial aux éclats de bacon, spécialité panée au poulet, bacon, fromage fondu goût bacon au poivre, oignons préparés et frits, salade, sauce', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1190kJ 	2762kJ 	33%\r\n284kcal 	660kcal 	33%\r\nMatières grasses 	15g 	34g 	49%\r\ndont acides gras saturés 	3g 	7g 	35%\r\nGlucides 	26g 	61g 	23%\r\ndont sucres 	3g 	7g 	8%\r\nFibres 	1.3g 	3g 	\r\nProtéines 	11g 	26g 	52%\r\nSel 	1.3g 	3g 	50%', 'allergen/product//1594717553.png', '2020-07-14 09:05:53', '2020-07-14 09:05:53'),
(8, 'LE FILET-O-FISH', 'Fondez pour son poisson pané croustillant et sa sauce légèrement vinaigrée aux oignons et aux câpres, le tout dans un pain cuit vapeur. Laissez-vous prendre dans ses filets !', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1015kJ 	1391kJ 	17%\r\n242kcal 	331kcal 	17%\r\nMatières grasses 	9.7g 	13g 	19%\r\ndont acides gras saturés 	1.9g 	2.6g 	13%\r\nGlucides 	27g 	37g 	14%\r\ndont sucres 	3.5g 	4.8g 	5%\r\nFibres 	1.4g 	2g 	\r\nProtéines 	11g 	15g 	31%\r\nSel 	1.1g 	1.5g 	24%', 'allergen/product//1594717752.png', '2020-07-14 09:09:12', '2020-07-14 09:09:12'),
(9, 'LE McFISH', 'Caché au fond du Happy Meal, le McFish gagne à être connu : un pain lisse autour d\'un poisson pané délicieusement croustillant et un peu de ketchup. Idéal pour s\'initier au poisson !', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	960kJ 	1191kJ 	14%\r\n228kcal 	283kcal 	14%\r\nMatières grasses 	6g 	7.4g 	11%\r\ndont acides gras saturés 	0.7g 	0.9g 	4%\r\nGlucides 	31g 	39g 	15%\r\ndont sucres 	6.4g 	7.9g 	9%\r\nFibres 	1.8g 	2.3g 	\r\nProtéines 	11g 	14g 	28%\r\nSel 	1.1g 	1.4g 	23%', 'allergen/product//1594718007.png', '2020-07-14 09:13:27', '2020-07-14 09:13:27'),
(10, 'LE HAMBURGER', 'Un indémodable, tout simplement\r\n\r\nUn steak haché, une rondelle de cornichon, des oignons, de la moutarde douce et du ketchup, retrouvez tout l\'esprit de McDonald\'s dans ce classique au goût inimitable.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1017kJ 	1063kJ 	13%\r\n242kcal 	253kcal 	13%\r\nMatières grasses 	8.2g 	8.5g 	12%\r\ndont acides gras saturés 	3.2g 	3.4g 	17%\r\nGlucides 	29g 	30g 	12%\r\ndont sucres 	6.1g 	6.4g 	7%\r\nFibres 	1.8g 	1.9g 	\r\nProtéines 	12g 	13g 	26%\r\nSel 	1.1g 	1.2g 	20%', 'allergen/product//1594718258.png', '2020-07-14 09:17:38', '2020-07-14 09:17:38'),
(11, 'LE CHEESEBURGER', 'Le « Cheese » pour les intimes\r\n\r\nUn steak haché, une tranche de cheddar fondu, une rondelle de cornichon, des oignons, du ketchup et de la moutarde douce dans un pain classique : fondez pour son goût unique et emblématique.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1067kJ 	1266kJ 	15%\r\n254kcal 	302kcal 	15%\r\nMatières grasses 	10g 	12g 	18%\r\ndont acides gras saturés 	5.1g 	6g 	30%\r\nGlucides 	26g 	31g 	12%\r\ndont sucres 	5.8g 	6.9g 	8%\r\nFibres 	1.6g 	1.9g 	\r\nProtéines 	13g 	16g 	31%\r\nSel 	1.4g 	1.6g 	27%', 'allergen/product//1594718381.png', '2020-07-14 09:19:41', '2020-07-14 09:19:41'),
(12, 'LE DOUBLE CHEESE', 'Double Cheese, double plaisir !\r\n\r\nUn goût culte : 2 steaks hachés cuits au grill, 2 tranches de cheddar fondu, 2 sauces ketchup et moutarde… 2 fois plus de plaisir !\r\n\r\nDouble Cheese = Double Cheeseburger', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1084kJ 	1849kJ 	22%\r\n259kcal 	443kcal 	22%\r\nMatières grasses 	13g 	22g 	32%\r\ndont acides gras saturés 	6.8g 	12g 	58%\r\nGlucides 	19g 	32g 	12%\r\ndont sucres 	4.3g 	7.4g 	8%\r\nFibres 	1.1g 	1.9g 	\r\nProtéines 	15g 	26g 	53%\r\nSel 	1.4g 	2.4g 	41%', 'allergen/product//1594718587.png', '2020-07-14 09:23:07', '2020-07-14 09:23:07'),
(13, 'LE TRIPLE CHEESEBURGER', 'Le Triple Cheeseburger, c\'est la recette culte du cheeseburger pour 3 fois plus de plaisir !\r\n\r\nUne recette simple mais irrésistible : Un pain bun, 3 tranches de cheddar fondu, 3 steaks hachés 100% pur bœuf*, des oignons, des cornichons et l\'iconique mélange ketchup-moutarde\r\n\r\nDurée limitée\r\n\r\n*comme tout steak haché', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1107kJ 	2469kJ 	30%\r\n265kcal 	590kcal 	30%\r\nMatières grasses 	15g 	34g 	49%\r\ndont acides gras saturés 	8.1g 	18g 	90%\r\nGlucides 	14g 	32g 	12%\r\ndont sucres 	4g 	8g 	9%\r\nFibres 	0.9g 	2g 	\r\nProtéines 	17g 	38g 	76%\r\nSel 	1.4g 	3.2g 	53%', 'allergen/product//1594718762.png', '2020-07-14 09:26:02', '2020-07-14 09:26:02'),
(14, 'LE TRIPLE CHEESBURGER BACON', 'Le Triple Cheeseburger Bacon, c\'est la recette culte du Triple Cheeseburger avec du bacon pour encore plus de plaisir !\r\n\r\nUne recette simple mais irrésistible : Un pain spécial , 3 tranches de cheddar fondu, 3 steaks hachés 100% pur bœuf*, du bacon fumé au bois de hêtre, des oignons, des cornichons et l\'iconique mélange ketchup-moutarde.\r\n\r\n*comme tout steak haché', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1150kJ 	2633kJ 	11%\r\n275kcal 	630kcal 	11%\r\nMatières grasses 	17g 	38g 	54%\r\ndont acides gras saturés 	8.2g 	19g 	94%\r\nGlucides 	14g 	31g 	12%\r\ndont sucres 	3.9g 	8.9g 	10%\r\nFibres 	0.9g 	2g 	\r\nProtéines 	17g 	40g 	80%\r\nSel 	1.4g 	3.3g 	55%', 'allergen/product//1594718949.png', '2020-07-14 09:29:09', '2020-07-14 09:29:09'),
(15, 'LE BLUE CHEESE & BACON', 'Le burger gourmet signé McDonald\'s™\r\n\r\nDécouvrez la recette : de la Fourme d\'Ambert AOP, de la viande Charolaise, du long bacon fumé au bois de hêtre, des oignons fondants, le tout dans un pain spécial. \r\n\r\nDurée Limitée\r\nBlue Cheese & Bacon = steak haché & Fourme d\'Ambert AOP', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1125kJ 	2925kJ 	35%\r\n269kcal 	699kcal 	35%\r\nMatières grasses 	14g 	37g 	53%\r\ndont acides gras saturés 	6.2g 	16g 	80%\r\nGlucides 	19g 	50g 	19%\r\ndont sucres 	3g 	8g 	9%\r\nFibres 	1.2g 	3g 	\r\nProtéines 	15g 	40g 	80%\r\nSel 	1g 	2.6g 	43%', 'allergen/product//1594719152.png', '2020-07-14 09:32:32', '2020-07-14 09:32:32'),
(16, 'LE DOUBLE BLUE CHEESE & BACON', 'Le burger gourmet signé McDonald\'s™\r\n\r\nDécouvrez la recette : de la Fourme d\'Ambert AOP, de la viande Charolaise, du long bacon fumé au bois de hêtre, des oignons fondants, le tout dans un pain spécial. \r\n\r\nDurée Limitée\r\nDouble Blue Cheese & Bacon = 2 steaks hachés & Fourme d\'Ambert AOP', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1119kJ 	3916kJ 	47%\r\n267kcal 	936kcal 	47%\r\nMatières grasses 	15g 	54g 	77%\r\ndont acides gras saturés 	6.9g 	24g 	120%\r\nGlucides 	15g 	51g 	20%\r\ndont sucres 	2g 	8g 	9%\r\nFibres 	0.9g 	3g 	\r\nProtéines 	17g 	60g 	120%\r\nSel 	0.91g 	3.2g 	53%', 'allergen/product//1594719427.png', '2020-07-14 09:37:07', '2020-07-14 09:37:07'),
(17, 'LA BOÎTE DE 20 CHICKEN McNUGGETS™', 'À partager sans plus attendre !\r\n\r\nCraquez pour ces Chicken McNuggets™ croustillants, à savourer avec 3 sauces au choix, en famille ou entre amis, faîtes-vous plaisir !\r\n\r\nChicken = spécialité panée au poulet.\r\nNos Chicken McNuggets™ sont préparés à partir de blanc de poulet origine France.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1043kJ 	3734kJ 	44%\r\n249kcal 	893kcal 	45%\r\nMatières grasses 	13g 	46g 	65%\r\ndont acides gras saturés 	1.5g 	5.5g 	28%\r\nGlucides 	17g 	62g 	24%\r\ndont sucres 	0.4g 	1.4g 	2%\r\nFibres 	1.3g 	4.6g 	\r\nProtéines 	16g 	56g 	112%\r\nSel 	1.2g 	4.2g 	71%', 'allergen/product//1594719592.png', '2020-07-14 09:39:52', '2020-07-14 09:39:52'),
(18, 'LE McWRAP™ NEW YORK & POULET BACON', 'Une recette très américaine avec un mariage qui fait chavirer nos papilles : du poulet croustillant et un délicieux bacon fumé au bois de hêtre. Ajoutez du cheddar fondu et une sauce onctueuse et ça y est : vous êtes à Time Square !\r\n\r\nDurée Limitée\r\nRecette inspirée de la cuisine new yorkaise avec une spécialité panée au poulet\r\nPoulet = Spécialité panée au poulet', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	982kJ 	2533kJ 	13%\r\n235kcal 	606kcal 	13%\r\nMatières grasses 	12g 	31g 	44%\r\ndont acides gras saturés 	2.7g 	7g 	35%\r\nGlucides 	20g 	53g 	20%\r\ndont sucres 	1.9g 	4.9g 	5%\r\nFibres 	1.3g 	3.4g 	\r\nProtéines 	11g 	28g 	55%\r\nSel 	0.8g 	2.1g 	34%', 'allergen/product//1594719753.png', '2020-07-14 09:42:33', '2020-07-14 09:42:33'),
(19, 'LE McWRAP™ VEGGIE', 'Deux palets panés aux légumes et à l\'emmental français , une sauce onctueuse, des oignons frits, une tranche d\'emmental, des rondelles de tomates, de la salade, le tout enveloppé dans une galette de blé.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1115kJ 	2552kJ 	31%\r\n266kcal 	610kcal 	31%\r\nMatières grasses 	15g 	34g 	49%\r\ndont acides gras saturés 	3.5g 	8g 	40%\r\nGlucides 	27g 	61g 	23%\r\ndont sucres 	4g 	9g 	10%\r\nFibres 	0.9g 	2g 	\r\nProtéines 	6.1g 	14g 	28%\r\nSel 	0.96g 	2.2g 	37%', 'allergen/product//1594794913.png', '2020-07-15 06:35:13', '2020-07-15 06:35:13'),
(20, 'LE McWRAP™ CHÈVRE', 'Deux chèvres croustillants et chauds, une sauce onctueuse, des oignons frits, des rondelles de tomates, de la salade, le tout enveloppé dans une galette de blé.\r\n\r\nChèvre = spécialité panée à base de fromage de chèvre', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1048kJ 	2243kJ 	27%\r\n250kcal 	536kcal 	27%\r\nMatières grasses 	12g 	26g 	37%\r\ndont acides gras saturés 	3.7g 	8g 	40%\r\nGlucides 	28g 	59g 	23%\r\ndont sucres 	2g 	4g 	4%\r\nFibres 	1.4g 	3g 	\r\nProtéines 	7g 	15g 	30%\r\nSel 	0.93g 	2g 	33%', 'allergen/product//1594796254.png', '2020-07-15 06:57:34', '2020-07-15 06:57:34'),
(21, 'McFIRST™ BŒUF', 'La version au bœuf du McFirst\r\n\r\nQu\'est-ce qui rend le McFirst Bœuf unique ? Deux steaks hachés, un pain spécial aux graines de sésame et de pavot, du cheddar fondu, une rondelle de tomate, de la salade, du ketchup ainsi que de la sauce ranch.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	901kJ 	1531kJ 	18%\r\n215kcal 	366kcal 	18%\r\nMatières grasses 	9g 	16g 	23%\r\ndont acides gras saturés 	3.5g 	6g 	30%\r\nGlucides 	22g 	37g 	14%\r\ndont sucres 	6g 	10g 	11%\r\nFibres 	1.8g 	3g 	\r\nProtéines 	10g 	17g 	34%\r\nSel 	1.1g 	1.8g 	30%', 'allergen/product//1594796536.png', '2020-07-15 07:02:17', '2020-07-15 07:02:17'),
(22, 'McFIRST™ POULET', 'La version au poulet du McFirst\r\n\r\nQu\'est-ce qui rend le McFirst Poulet unique ? Une délicieuse spécialité panée au poulet, un pain spécial aux graines de sésame et de pavot, du cheddar fondu, une rondelle de tomate, de la salade, du ketchup ainsi que de la sauce ranch.\r\n\r\nPoulet : spécialité panée au poulet', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	865kJ 	1824kJ 	22%\r\n207kcal 	436kcal 	22%\r\nMatières grasses 	9g 	18g 	26%\r\ndont acides gras saturés 	2.4g 	5g 	25%\r\nGlucides 	22g 	46g 	18%\r\ndont sucres 	5g 	10g 	11%\r\nFibres 	1.4g 	3g 	\r\nProtéines 	10g 	21g 	42%\r\nSel 	1.3g 	2.7g 	45%', 'allergen/product//1594796899.png', '2020-07-15 07:08:19', '2020-07-15 07:08:19'),
(23, 'McFIRST™ POISSON', 'La version au poisson du McFirst\r\n\r\nQu\'est-ce qui rend le McFirst Poisson unique ? Une délicieuse préparation de poisson pané sublimée par un pain spécial aux graines de sésame et de pavot, du cheddar fondu, une rondelle de tomate, de la salade, du ketchup ainsi que de la sauce ranch.\r\n\r\nPoisson: preparation de poisson pané', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	841kJ 	1607kJ 	19%\r\n201kcal 	384kcal 	19%\r\nMatières grasses 	7g 	14g 	20%\r\ndont acides gras saturés 	2.1g 	4g 	20%\r\nGlucides 	24g 	45g 	17%\r\ndont sucres 	5g 	10g 	11%\r\nFibres 	1.6g 	3g 	\r\nProtéines 	9.4g 	18g 	36%\r\nSel 	0.99g 	1.9g 	32%', 'allergen/product//1594797109.png', '2020-07-15 07:11:49', '2020-07-15 07:11:49'),
(24, 'LE McMUFFIN™ EGG & CHEESE', 'Le plus doux des burgers\r\n\r\nOffrez vous un moment de douceur tout au long de la journée en découvrant l\'accord parfait entre un pain muffin délicatement toasté, un oeuf origine France cassé et cuit en restaurant issu de poules élevées en plein air et du cheddar fondu.\r\n\r\n\r\nLe McMuffin™ Egg & Cheese est une alternative végétarienne disponible à la carte mais aussi dans le Menu Happy Meal™.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	846kJ 	1134kJ 	14%\r\n202kcal 	271kcal 	14%\r\nMatières grasses 	8g 	11g 	16%\r\ndont acides gras saturés 	3.7g 	5g 	25%\r\nGlucides 	19g 	26g 	10%\r\ndont sucres 	2g 	3g 	3%\r\nFibres 	1.5g 	2g 	\r\nProtéines 	12g 	16g 	32%\r\nSel 	0.97g 	1.3g 	22%', 'allergen/product//1594797310.png', '2020-07-15 07:15:11', '2020-07-15 07:15:11'),
(25, 'LE P\'TIT RANCH', 'Une recette gourmande pour les petites faims\r\n\r\nUne galette de blé garnie de poulet croustillant*, du cheddar fondu et des oignons frits, le tout accompagné d\'une sauce onctueuse.\r\n\r\nP\'tit wrap= p\'tit roulé.\r\n*Spécialité panée au poulet.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1030kJ 	1319kJ 	16%\r\n246kcal 	315kcal 	16%\r\nMatières grasses 	11g 	14g 	20%\r\ndont acides gras saturés 	2.3g 	3g 	15%\r\nGlucides 	28g 	36g 	14%\r\ndont sucres 	2g 	2g 	2%\r\nFibres 	1.6g 	2g 	\r\nProtéines 	9.4g 	12g 	24%\r\nSel 	1.4g 	1.8g 	30%', 'allergen/product//1594797477.png', '2020-07-15 07:17:57', '2020-07-15 07:17:57'),
(26, 'LE CROQUE McDo™', 'Un croque monsieur tout rond, tout en simplicité : deux tranches d\'emmental fondu, une tranche de jambon supérieur, dans un pain retourné et toasté. Il ne lui en faut pas plus pour être b', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1140kJ 	1083kJ 	13%\r\n272kcal 	258kcal 	13%\r\nMatières grasses 	10g 	9.9g 	14%\r\ndont acides gras saturés 	6g 	5.7g 	29%\r\nGlucides 	28g 	27g 	10%\r\ndont sucres 	5.5g 	5.2g 	6%\r\nFibres 	2.1g 	2g 	\r\nProtéines 	15g 	14g 	28%\r\nSel 	2g 	1.9g 	32%', 'allergen/product//1594797644.png', '2020-07-15 07:20:44', '2020-07-15 07:20:44'),
(27, 'LE P\'TIT FONDU', 'Faîtes vous un P\'tit Plaisir avec le P\'tit Fondu et son onctueuse sauce à la raclette fondue !\r\n\r\nAvec sa tranche d\'emmental fondu, sa sauce à la raclette fondue, ses oignons frits et son steak haché 100% pur bœuf*, fondez pour le P\'tit Fondu !\r\n\r\nDurée limitée', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	689kJ 	778kJ 	9%\r\n165kcal 	186kcal 	9%\r\nMatières grasses 	12g 	14g 	20%\r\ndont acides gras saturés 	5.3g 	6g 	30%\r\nGlucides 	4g 	4g 	2%\r\ndont sucres 	2g 	2g 	2%\r\nFibres 	0g 	0g 	\r\nProtéines 	9.7g 	11g 	22%\r\nSel 	0.88g 	1g 	17%', 'allergen/product//1594797783.png', '2020-07-15 07:23:03', '2020-07-15 07:23:03'),
(28, 'LE McFLURRY', 'Inventez le McFlurry™ de vos rêves !\r\nUn délicieux tourbillon glacé* accompagné d\'une gourmandise et d\'un nappage pour un plaisir intense !\r\nChoisissez un croquant : Oréo®, Speculoos Lotus, Daim®, Kit Kat Ball® ou M&M\'s®**.\r\nComplétez avec le nouveau coulis fraise ou un nappage saveur chocolat ou caramel.\r\n \r\nTout simplement irrésistible !', NULL, 'allergen/product//1594798056.png', '2020-07-15 07:27:36', '2020-07-15 07:27:36'),
(29, 'LE SUNDAE', 'Craquez pour un plaisir glacé au lait avec des éclats de cacahuètes et retrouvez les nappages saveur caramel, chocolat ou coulis fraise, pour une pause gourmande ou un plaisir de fin de repas.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	786kJ 	1207kJ 	14%\r\n186kcal 	286kcal 	14%\r\nMatières grasses 	3.9g 	6.1g 	9%\r\ndont acides gras saturés 	2.7g 	4.1g 	20%\r\nGlucides 	35g 	53g 	20%\r\ndont sucres 	25g 	38g 	42%\r\nFibres 	0g 	0g 	\r\nProtéines 	3g 	4.7g 	9%\r\nSel 	0.18g 	0.28g 	5%', 'allergen/product//1594798249.png', '2020-07-15 07:30:49', '2020-07-15 07:30:49'),
(30, 'LE VERY PARFAIT', 'Un coulis fraise 100% gourmand ! Et retrouvez toute la gamme Very Parfait : nappages saveur chocolat ou caramel ou le nouveau coulis de fraise.\r\n\r\nCoulis fraise = coulis à la fraise et au jus de citron\r\nVery Parfait = Si parfait.\r\nDurée limitée.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	611kJ 	1021kJ 	12%\r\n146kcal 	244kcal 	12%\r\nMatières grasses 	6g 	10g 	14%\r\ndont acides gras saturés 	4.2g 	7g 	35%\r\nGlucides 	20g 	34g 	13%\r\ndont sucres 	19g 	32g 	36%\r\nFibres 	0.6g 	1g 	\r\nProtéines 	2.4g 	4g 	8%\r\nSel 	0.12g 	0.2g 	3%', 'allergen/product//1594798417.png', '2020-07-15 07:33:37', '2020-07-15 07:33:37'),
(31, 'P\'TIT GLACÉ SAVEUR VANILLE', 'Un dessert glacé saveur vanille pour une touche de douceur dans ton Happy Meal !\r\n\r\nTube à presser contenant un dessert glacé à base de lait saveur vanille.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	614kJ 	276kJ 	3%\r\n147kcal 	66kcal 	3%\r\nMatières grasses 	4g 	2g 	3%\r\ndont acides gras saturés 	2.2g 	1g 	5%\r\nGlucides 	22g 	10g 	4%\r\ndont sucres 	20g 	9g 	10%\r\nFibres 	0g 	0g 	\r\nProtéines 	4.4g 	2g 	4%\r\nSel 	0.22g 	0.1g 	2%', 'allergen/product//1594798561.png', '2020-07-15 07:36:02', '2020-07-15 07:36:02'),
(32, 'LA P\'TITE POMME', 'Une touche fruitée à croquer\r\n\r\nApportez une note fruitée à votre repas. Craquez pour les savoureux quartiers de pommes* croquants.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	225kJ 	180kJ 	2%\r\n53kcal 	42kcal 	2%\r\nMatières grasses 	0.2g 	0.1g 	0%\r\ndont acides gras saturés 	0g 	0g 	0%\r\nGlucides 	13g 	11g 	4%\r\ndont sucres 	11g 	9g 	10%\r\nFibres 	2g 	1.6g 	\r\nProtéines 	0.3g 	0.2g 	0%\r\nSel 	0.1g 	0.1g 	2%', 'allergen/product//1594798744.png', '2020-07-15 07:39:04', '2020-07-15 07:39:04'),
(33, 'LE MUFFIN SAVEUR CHOCOLAT NOISETTE', 'Un délicieux muffin saveur chocolat avec des éclats de noisettes croquantes.\r\n\r\nProduit décongelé, ne pas recongeler.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1914kJ 	2488kJ 	30%\r\n458kcal 	595kcal 	30%\r\nMatières grasses 	26g 	33g 	48%\r\ndont acides gras saturés 	4.4g 	5.7g 	29%\r\nGlucides 	49g 	64g 	25%\r\ndont sucres 	32g 	42g 	47%\r\nFibres 	2.9g 	3.8g 	\r\nProtéines 	6.2g 	8.1g 	16%\r\nSel 	0.83g 	1.1g 	18%', 'allergen/product//1594798934.png', '2020-07-15 07:42:14', '2020-07-15 07:42:14'),
(34, 'LE COOKIE FOURRÉ CHOCONUTS', 'Envie d\'une pause gourmande ? Craquez pour le cookie fourré Choconuts, aux pépites de chocolat noir et aux morceaux de noisettes, fourré au chocolat et à la noisette.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1998kJ 	1598kJ 	19%\r\n478kcal 	382kcal 	19%\r\nMatières grasses 	23g 	18g 	26%\r\ndont acides gras saturés 	6.3g 	5g 	25%\r\nGlucides 	61g 	49g 	19%\r\ndont sucres 	38g 	30g 	33%\r\nFibres 	2.5g 	2g 	\r\nProtéines 	6.3g 	5g 	10%\r\nSel 	0.75g 	0.6g 	10%', 'allergen/product//1594799125.png', '2020-07-15 07:45:25', '2020-07-15 07:45:25'),
(35, 'LE MUFFIN AUX MYRTILLES', 'Découvrez le Muffin avec un fourrage à la myrtille.\r\n\r\nDurée limitée', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1582kJ 	2009kJ 	24%\r\n378kcal 	480kcal 	24%\r\nMatières grasses 	18g 	22g 	32%\r\ndont acides gras saturés 	8.5g 	11g 	54%\r\nGlucides 	50g 	63g 	24%\r\ndont sucres 	28g 	35g 	39%\r\nFibres 	0.8g 	1g 	\r\nProtéines 	4.8g 	6.1g 	12%\r\nSel 	0.89g 	1.1g 	19%', 'allergen/product//1594799308.png', '2020-07-15 07:48:28', '2020-07-15 07:48:28'),
(36, 'LE BROWNIE CHOCOLAT-NOISETTES', 'Fondez pour ce brownie au chocolat et morceaux de noisettes.\r\n\r\nProduit décongelé, ne pas recongeler.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1831kJ 	1557kJ 	19%\r\n438kcal 	372kcal 	19%\r\nMatières grasses 	26g 	22g 	31%\r\ndont acides gras saturés 	12g 	10g 	50%\r\nGlucides 	45g 	38g 	15%\r\ndont sucres 	15g 	13g 	14%\r\nFibres 	1.2g 	1g 	\r\nProtéines 	5.9g 	5g 	10%\r\nSel 	0.59g 	0.5g 	8%', 'allergen/product//1594799459.png', '2020-07-15 07:50:59', '2020-07-15 07:50:59'),
(37, 'LE DONUT NATURE', 'Découvrez le délicieux donut nature.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1847kJ 	1201kJ 	14%\r\n442kcal 	287kcal 	14%\r\nMatières grasses 	26g 	17g 	24%\r\ndont acides gras saturés 	12g 	8g 	40%\r\nGlucides 	45g 	29g 	11%\r\ndont sucres 	15g 	10g 	11%\r\nFibres 	1.5g 	1g 	\r\nProtéines 	6.2g 	4g 	8%\r\nSel 	0.62g 	0.4g 	7%', 'allergen/product//1594799626.png', '2020-07-15 07:53:46', '2020-07-15 07:53:46'),
(38, 'LA CLASSIC CAESAR', 'Découvrez la Classic Caesar, découpée et préparée chaque jour sur place.\r\n \r\nLa Classic Caesar est composée d\'émincés de filet de poulet mariné et rôti* origine France, de Grana Padano AOP, de croûtons croustillants et d\'un mélange de salades.\r\nL\'incontournable sauce Caesar accompagne cette recette à la perfection.\r\n \r\nNos Salades du Jour sont accompagnées d\'un petit pain bio lorsqu\'elles sont commandées seules ou en menu Salade & Boisson. Nos Salades du Jour sont également disponibles en menu Best-Of™ et Maxi Best-Of™.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	574kJ 	1268kJ 	15%\r\n137kcal 	303kcal 	15%\r\nMatières grasses 	6g 	13g 	19%\r\ndont acides gras saturés 	3.2g 	7g 	35%\r\nGlucides 	5g 	11g 	4%\r\ndont sucres 	1g 	2g 	2%\r\nFibres 	1.4g 	3g 	\r\nProtéines 	15g 	34g 	68%\r\nSel 	0.63g 	1.4g 	23%', 'allergen/product//1594800207.png', '2020-07-15 08:03:27', '2020-07-15 08:03:27'),
(39, 'L\'ITALIAN MOZZA ET PASTA', 'Découvrez l\'Italian Mozza & Pasta, découpée et préparée chaque jour sur place.\r\n \r\nL\'Italian Mozza & Pasta est composée d\'une mozzarella au lait de bufflonne, de pâtes radiatori, de tomates assaisonnées et d\'un mélange de salades.\r\nLa sauce vinaigrette aux herbes aromatiques accompagne cette recette à la perfection.\r\n \r\nNos Salades du Jour sont accompagnées d\'un petit pain bio lorsqu\'elles sont commandées seules ou en menu Salade & Boisson. Nos Salades du Jour sont également disponibles en menu Best-Of™ et Maxi Best-Of™.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	378kJ 	1021kJ 	12%\r\n90kcal 	244kcal 	12%\r\nMatières grasses 	7g 	18g 	26%\r\ndont acides gras saturés 	3.3g 	9g 	45%\r\nGlucides 	3g 	9g 	3%\r\ndont sucres 	0g 	1g 	1%\r\nFibres 	1.1g 	3g 	\r\nProtéines 	3.7g 	10g 	20%\r\nSel 	0.11g 	0.3g 	5%', 'allergen/product//1594800343.png', '2020-07-15 08:05:43', '2020-07-15 08:05:43'),
(40, 'LA TASTY BLUE CHEESE & BACON', 'Découvrez la Tasty Blue Cheese & Bacon, une recette gourmande, préparée et découpée chaque jour sur place.\r\n \r\nLa Tasty Blue Cheese & Bacon est composée d\'émincés de poulet mariné et rôti* origine France, de Fourme d\'Ambert* AOP, de bacon* origine France et fumé au bois de hêtre, de cerneaux de noix de pécan, de cranberries séchées, de tomates fraiches et d\'un mélange de salades.\r\nLa sauce au bleu fondu accompagne cette recette à la perfection.\r\n \r\nNos Salades du Jour sont accompagnées d\'un petit pain bio lorsqu\'elles sont commandées seules ou en menu Salade & Boisson. Nos Salades du Jour sont également disponibles en menu Best-Of™ et Maxi Best-Of™.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	609kJ 	1803kJ 	22%\r\n146kcal 	431kcal 	22%\r\nMatières grasses 	8g 	25g 	36%\r\ndont acides gras saturés 	3g 	9g 	45%\r\nGlucides 	5g 	15g 	6%\r\ndont sucres 	4g 	12g 	13%\r\nFibres 	1.7g 	5g 	\r\nProtéines 	12g 	34g 	68%\r\nSel 	0.64g 	1.9g 	32%', 'allergen/product//1594800487.png', '2020-07-15 08:08:07', '2020-07-15 08:08:07'),
(41, 'LE DONUT CONFETTI', 'Découvrez le donut confetti avec son délicieux nappage parsemé de vermicelles en sucre !\r\n\r\nDurée limitée\r\nDonut confetti = donut parsemé de vermicelles en sucre\r\nProduit décongelé, ne pas recongeler.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1789kJ 	1025kJ 	12%\r\n429kcal 	245kcal 	12%\r\nMatières grasses 	27g 	15g 	21%\r\ndont acides gras saturés 	14g 	8g 	40%\r\nGlucides 	41g 	23g 	9%\r\ndont sucres 	23g 	13g 	14%\r\nFibres 	1.5g 	1g 	\r\nProtéines 	7.1g 	4g 	8%\r\nSel 	1.2g 	0.7g 	12%', 'allergen/product//1594800666.png', '2020-07-15 08:11:06', '2020-07-15 08:11:06'),
(42, 'LES FRITES', 'En menu, pour combler une petite faim, en petite, moyenne ou bien grande portion, goûtez-les croustillantes et savoureuses.\r\n\r\nDisponibles en petite, moyenne ou grande portion.\r\n\r\nValeurs nutritionnelles présentées pour une petite portion.\r\nBâtonnets de pommes de terre frites.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	1210kJ 	968kJ 	12%\r\n289kcal 	231kcal 	12%\r\nMatières grasses 	14g 	11g 	16%\r\ndont acides gras saturés 	1.3g 	1g 	5%\r\nGlucides 	36g 	29g 	11%\r\ndont sucres 	0.3g 	0.3g 	0%\r\nFibres 	3.5g 	2.8g 	\r\nProtéines 	3.4g 	2.8g 	6%\r\nSel 	0.47g 	0.37g 	6%', 'allergen/product//1594825805.png', '2020-07-15 15:10:05', '2020-07-15 15:10:05'),
(43, 'LE BERLINGO\' FRUITS', 'Spécialité de fruits\r\nVoir le parfum du moment', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	213kJ 	213kJ 	4%\r\n50kcal 	50kcal 	10%\r\nMatières grasses 	0g 	0g 	0%\r\ndont acides gras saturés 	0g 	0g 	0%\r\nGlucides 	12g 	12g 	4%\r\ndont sucres 	11g 	11g 	12%\r\nFibres 	1.1g 	1.1g 	\r\nProtéines 	0g 	0g 	0%\r\nSel 	0g 	0g 	0%', 'allergen/product//1594827256.png', '2020-07-15 15:34:16', '2020-07-15 15:34:16'),
(44, 'BIO À BOIRE VANILLE', 'Un yaourt bio à déguster tout en s\'amusant', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	276kJ 	250kJ 	3%\r\n66kcal 	59kcal 	3%\r\nMatières grasses 	1.7g 	1.5g 	2%\r\ndont acides gras saturés 	1.1g 	1g 	5%\r\nGlucides 	9.6g 	8.6g 	3%\r\ndont sucres 	9.4g 	8.5g 	10%\r\nFibres 	0g 	0g 	\r\nProtéines 	2.9g 	2.6g 	5%\r\nSel 	0.13g 	0.09g 	2%', 'allergen/product//1594827397.png', '2020-07-15 15:36:37', '2020-07-15 15:36:37'),
(45, 'MINUTE MAID® ORANGE', 'A tout moment de la journée, Minute Maid vous apporte une délicieuse dose de vitalité avec son jus d\'orange à base de concentré avec pulpe.\r\n\r\nFaites-vous plaisir et comblez votre envie de fruits grâce à Minute Maid Orange. Ce jus à base de concentré est 100% teneur en fruits, sans sucres ajoutés, sans colorant et sans conservateur (conformément à la réglementation, contient naturellement le sucre des fruits).\r\n\r\nLe saviez-vous ? Boire un verre de Minute Maid Orange vous apporte l\'équivalent d\'une des 5 portions de fruits et légumes recommandées par jour. Minute Maid Orange est un jus à base de concentré.\r\n\r\nMinute Maid est une marque déposée de The Coca-Cola Company.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	213kJ 	426kJ 	5%\r\n50kcal 	100kcal 	5%\r\nMatières grasses 	0.2g 	0.4g 	1%\r\ndont acides gras saturés 	0g 	0g 	0%\r\nGlucides 	9.2g 	18g 	7%\r\ndont sucres 	8.6g 	17g 	19%\r\nFibres 	1.8g 	3.6g 	\r\nProtéines 	1.1g 	2.2g 	4%\r\nSel 	0g 	0g 	0%', 'allergen/product//1594827668.png', '2020-07-15 15:41:08', '2020-07-15 15:41:08'),
(46, 'NECTAR DE POMME BIO', 'Pratique et ludique, le Nectar Bio convient aux plus petits comme aux plus grands !\r\nLe Nectar Bio pomme, sans sucres ajoutés, est à base de jus de pomme issu de l\'agriculture biologique.', 'POUR 100G 	PAR PRODUIT 	APPORT DE RÉFÉRENCE (AR*)\r\nÉnergie 	135kJ 	270kJ 	3%\r\n32kcal 	64kcal 	3%\r\nMatières grasses 	0g 	0.3g 	0%\r\ndont acides gras saturés 	0g 	0g 	0%\r\nGlucides 	7g 	15g 	6%\r\ndont sucres 	7g 	14g 	16%\r\nFibres 	0.2g 	0.3g 	\r\nProtéines 	0.2g 	0.4g 	1%\r\nSel 	0g 	0g 	0%', 'allergen/product//1594827878.png', '2020-07-15 15:44:38', '2020-07-15 15:44:38');

--
-- Déclencheurs `products`
--
DROP TRIGGER IF EXISTS `tr_remove_non_referenced_product_images_on_delete`;
DELIMITER $$
CREATE TRIGGER `tr_remove_non_referenced_product_images_on_delete` AFTER DELETE ON `products` FOR EACH ROW BEGIN
                IF OLD.picture THEN
                    INSERT INTO to_remove_from_server(path) VALUES (OLD.picture);
                END IF;
            END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `tr_remove_non_referenced_product_images_on_updte`;
DELIMITER $$
CREATE TRIGGER `tr_remove_non_referenced_product_images_on_updte` AFTER UPDATE ON `products` FOR EACH ROW BEGIN
                IF NEW.picture != OLD.picture THEN
                    INSERT INTO to_remove_from_server(path) VALUES (OLD.picture);
                END IF;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `product_has_allergen`
--

DROP TABLE IF EXISTS `product_has_allergen`;
CREATE TABLE IF NOT EXISTS `product_has_allergen` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `allergen_id` bigint(20) UNSIGNED NOT NULL,
  `is_trace` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_product_a_allergens_a_product_id_foreign` (`product_id`),
  KEY `a_product_a_allergens_allergen_id_foreign` (`allergen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_has_allergen`
--

INSERT INTO `product_has_allergen` (`id`, `product_id`, `allergen_id`, `is_trace`, `created_at`, `updated_at`) VALUES
(6, 2, 15, 0, NULL, NULL),
(7, 2, 7, 0, NULL, NULL),
(8, 2, 13, 0, NULL, NULL),
(9, 2, 4, 0, NULL, NULL),
(10, 2, 1, 0, NULL, NULL),
(11, 3, 7, 0, NULL, NULL),
(12, 3, 8, 0, NULL, NULL),
(13, 3, 15, 0, NULL, NULL),
(14, 3, 13, 0, NULL, NULL),
(15, 3, 1, 0, NULL, NULL),
(16, 3, 4, 0, NULL, NULL),
(17, 4, 15, 0, NULL, NULL),
(18, 4, 13, 0, NULL, NULL),
(19, 4, 4, 0, NULL, NULL),
(20, 4, 1, 0, NULL, NULL),
(21, 5, 15, 0, NULL, NULL),
(22, 5, 13, 0, NULL, NULL),
(23, 5, 1, 0, NULL, NULL),
(24, 6, 15, 0, NULL, NULL),
(25, 6, 13, 0, NULL, NULL),
(26, 6, 1, 0, NULL, NULL),
(27, 7, 15, 0, NULL, NULL),
(28, 7, 4, 0, NULL, NULL),
(29, 7, 13, 0, NULL, NULL),
(30, 7, 1, 0, NULL, NULL),
(31, 8, 8, 0, NULL, NULL),
(32, 8, 15, 0, NULL, NULL),
(33, 8, 13, 0, NULL, NULL),
(34, 8, 4, 0, NULL, NULL),
(35, 8, 1, 0, NULL, NULL),
(36, 8, 14, 0, NULL, NULL),
(37, 8, 9, 0, NULL, NULL),
(38, 9, 8, 0, NULL, NULL),
(39, 9, 15, 0, NULL, NULL),
(40, 9, 9, 0, NULL, NULL),
(41, 9, 14, 0, NULL, NULL),
(42, 9, 13, 0, NULL, NULL),
(43, 10, 4, 0, NULL, NULL),
(44, 11, 7, 0, NULL, NULL),
(45, 11, 15, 0, NULL, NULL),
(46, 11, 13, 0, NULL, NULL),
(47, 11, 4, 0, NULL, NULL),
(48, 12, 7, 0, NULL, NULL),
(49, 12, 15, 0, NULL, NULL),
(50, 12, 13, 0, NULL, NULL),
(51, 12, 4, 0, NULL, NULL),
(52, 13, 7, 0, NULL, NULL),
(53, 13, 15, 0, NULL, NULL),
(54, 13, 13, 0, NULL, NULL),
(55, 13, 4, 0, NULL, NULL),
(56, 14, 7, 0, NULL, NULL),
(57, 14, 15, 0, NULL, NULL),
(58, 14, 13, 0, NULL, NULL),
(59, 14, 4, 0, NULL, NULL),
(60, 15, 15, 0, NULL, NULL),
(61, 15, 13, 0, NULL, NULL),
(62, 15, 1, 0, NULL, NULL),
(63, 16, 15, 0, NULL, NULL),
(64, 16, 13, 0, NULL, NULL),
(65, 16, 1, 0, NULL, NULL),
(66, 17, 15, 0, NULL, NULL),
(67, 17, 8, 0, NULL, NULL),
(68, 18, 15, 0, NULL, NULL),
(69, 18, 4, 0, NULL, NULL),
(70, 18, 1, 0, NULL, NULL),
(71, 19, 8, 0, NULL, NULL),
(72, 19, 15, 0, NULL, NULL),
(73, 19, 1, 0, NULL, NULL),
(74, 19, 13, 0, NULL, NULL),
(75, 19, 9, 0, NULL, NULL),
(76, 19, 4, 0, NULL, NULL),
(77, 20, 8, 0, NULL, NULL),
(78, 20, 1, 0, NULL, NULL),
(79, 20, 13, 0, NULL, NULL),
(80, 20, 9, 0, NULL, NULL),
(81, 20, 4, 0, NULL, NULL),
(82, 21, 15, 0, NULL, NULL),
(83, 21, 13, 0, NULL, NULL),
(84, 21, 1, 0, NULL, NULL),
(85, 22, 8, 0, NULL, NULL),
(86, 22, 15, 0, NULL, NULL),
(87, 22, 1, 0, NULL, NULL),
(88, 22, 13, 0, NULL, NULL),
(89, 22, 4, 0, NULL, NULL),
(90, 23, 15, 0, NULL, NULL),
(91, 23, 13, 0, NULL, NULL),
(92, 23, 1, 0, NULL, NULL),
(93, 23, 14, 0, NULL, NULL),
(94, 24, 13, 0, NULL, NULL),
(95, 24, 1, 0, NULL, NULL),
(96, 25, 13, 0, NULL, NULL),
(97, 25, 1, 0, NULL, NULL),
(98, 26, 13, 0, NULL, NULL),
(99, 26, 15, 0, NULL, NULL),
(100, 27, 13, 0, NULL, NULL),
(101, 27, 1, 0, NULL, NULL),
(102, 29, 5, 0, NULL, NULL),
(103, 29, 13, 0, NULL, NULL),
(104, 30, 13, 0, NULL, NULL),
(105, 30, 7, 0, NULL, NULL),
(106, 31, 13, 0, NULL, NULL),
(107, 33, 1, 0, NULL, NULL),
(108, 33, 13, 0, NULL, NULL),
(109, 33, 9, 0, NULL, NULL),
(110, 33, 12, 0, NULL, NULL),
(111, 34, 7, 0, NULL, NULL),
(112, 34, 12, 0, NULL, NULL),
(113, 34, 13, 0, NULL, NULL),
(114, 34, 1, 0, NULL, NULL),
(115, 34, 9, 0, NULL, NULL),
(116, 35, 13, 0, NULL, NULL),
(117, 35, 1, 0, NULL, NULL),
(118, 35, 9, 0, NULL, NULL),
(119, 36, 12, 0, NULL, NULL),
(120, 36, 15, 0, NULL, NULL),
(121, 36, 13, 0, NULL, NULL),
(122, 36, 1, 0, NULL, NULL),
(123, 36, 9, 0, NULL, NULL),
(124, 37, 12, 0, NULL, NULL),
(125, 37, 13, 0, NULL, NULL),
(126, 37, 1, 0, NULL, NULL),
(127, 37, 9, 0, NULL, NULL),
(128, 38, 1, 0, NULL, NULL),
(129, 38, 13, 0, NULL, NULL),
(130, 39, 13, 0, NULL, NULL),
(131, 39, 1, 0, NULL, NULL),
(132, 40, 12, 0, NULL, NULL),
(133, 40, 13, 0, NULL, NULL),
(134, 41, 13, 0, NULL, NULL),
(135, 41, 9, 0, NULL, NULL),
(136, 39, 11, 0, NULL, NULL),
(137, 25, 11, 0, NULL, NULL),
(138, 4, 11, 0, NULL, NULL),
(139, 5, 11, 0, NULL, NULL),
(140, 44, 13, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `product_has_criteria`
--

DROP TABLE IF EXISTS `product_has_criteria`;
CREATE TABLE IF NOT EXISTS `product_has_criteria` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `criteria_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_product_a_criteria_a_product_id_foreign` (`product_id`),
  KEY `a_product_a_criteria_a_criteria_id_foreign` (`criteria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_has_criteria`
--

INSERT INTO `product_has_criteria` (`id`, `product_id`, `criteria_id`, `created_at`, `updated_at`) VALUES
(1, 39, 3, NULL, NULL),
(2, 32, 2, NULL, NULL),
(3, 32, 3, NULL, NULL),
(4, 36, 3, NULL, NULL),
(5, 34, 3, NULL, NULL),
(6, 20, 3, NULL, NULL),
(7, 19, 3, NULL, NULL),
(8, 45, 3, NULL, NULL),
(9, 44, 3, NULL, NULL),
(10, 43, 3, NULL, NULL),
(11, 43, 2, NULL, NULL),
(12, 37, 3, NULL, NULL),
(13, 41, 3, NULL, NULL),
(14, 28, 3, NULL, NULL),
(15, 42, 3, NULL, NULL),
(16, 30, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `product_has_ingredient`
--

DROP TABLE IF EXISTS `product_has_ingredient`;
CREATE TABLE IF NOT EXISTS `product_has_ingredient` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `ingredient_id` bigint(20) UNSIGNED NOT NULL,
  `is_major_ingredient` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_product_a_ingredients_a_product_id_foreign` (`product_id`),
  KEY `a_product_a_ingredients_a_ingredient_id_foreign` (`ingredient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_has_ingredient`
--

INSERT INTO `product_has_ingredient` (`id`, `product_id`, `ingredient_id`, `is_major_ingredient`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 0, NULL, NULL),
(3, 17, 3, 1, NULL, NULL),
(4, 6, 3, 1, NULL, NULL),
(5, 3, 3, 1, NULL, NULL),
(6, 18, 3, 1, NULL, NULL),
(9, 16, 1, 1, NULL, NULL),
(10, 15, 2, 1, NULL, NULL),
(11, 12, 1, 1, NULL, NULL),
(12, 14, 1, 1, NULL, NULL),
(13, 7, 3, 1, NULL, NULL),
(14, 8, 4, 0, NULL, NULL),
(15, 9, 4, 1, NULL, NULL),
(19, 23, 4, 0, NULL, NULL),
(21, 21, 7, 1, NULL, NULL),
(23, 22, 3, 1, NULL, NULL),
(24, 25, 3, 1, NULL, NULL),
(25, 13, 7, 1, NULL, NULL),
(26, 13, 1, 1, NULL, NULL),
(27, 15, 7, 1, NULL, NULL),
(28, 4, 7, 1, NULL, NULL),
(29, 5, 7, 1, NULL, NULL),
(30, 42, 22, 1, NULL, NULL),
(31, 44, 23, 1, NULL, NULL),
(32, 45, 24, 1, NULL, NULL),
(33, 46, 25, 1, NULL, NULL),
(35, 38, 3, 1, NULL, NULL),
(36, 40, 10, 1, NULL, NULL),
(38, 39, 10, 1, NULL, NULL),
(39, 38, 10, 1, NULL, NULL),
(40, 32, 26, 1, NULL, NULL),
(41, 4, 13, 1, NULL, NULL),
(42, 43, 26, 1, NULL, NULL),
(43, 2, 12, 1, NULL, NULL),
(44, 5, 12, 1, NULL, NULL),
(46, 15, 12, 1, NULL, NULL),
(47, 7, 13, 1, NULL, NULL),
(48, 11, 12, 1, NULL, NULL),
(49, 11, 1, 1, NULL, NULL),
(50, 11, 16, 0, NULL, NULL),
(51, 16, 12, 1, NULL, NULL),
(52, 9, 12, 1, NULL, NULL),
(53, 40, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `product_has_label`
--

DROP TABLE IF EXISTS `product_has_label`;
CREATE TABLE IF NOT EXISTS `product_has_label` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `label_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a_product_a_label_a_product_id_foreign` (`product_id`),
  KEY `a_product_a_label_a_label_id_foreign` (`label_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_has_label`
--

INSERT INTO `product_has_label` (`id`, `product_id`, `label_id`, `created_at`, `updated_at`) VALUES
(1, 44, 1, NULL, NULL),
(2, 46, 1, NULL, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ingredient_has_allergen`
--
ALTER TABLE `ingredient_has_allergen`
  ADD CONSTRAINT `a_ingredient_a_allergens_a_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `a_ingredient_a_allergens_allergen_id_foreign` FOREIGN KEY (`allergen_id`) REFERENCES `allergens` (`id`);

--
-- Contraintes pour la table `product_has_allergen`
--
ALTER TABLE `product_has_allergen`
  ADD CONSTRAINT `a_product_a_allergens_a_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `a_product_a_allergens_allergen_id_foreign` FOREIGN KEY (`allergen_id`) REFERENCES `allergens` (`id`);

--
-- Contraintes pour la table `product_has_criteria`
--
ALTER TABLE `product_has_criteria`
  ADD CONSTRAINT `a_product_a_criteria_a_criteria_id_foreign` FOREIGN KEY (`criteria_id`) REFERENCES `criterias` (`id`),
  ADD CONSTRAINT `a_product_a_criteria_a_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `product_has_ingredient`
--
ALTER TABLE `product_has_ingredient`
  ADD CONSTRAINT `a_product_a_ingredients_a_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  ADD CONSTRAINT `a_product_a_ingredients_a_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `product_has_label`
--
ALTER TABLE `product_has_label`
  ADD CONSTRAINT `a_product_a_label_a_label_id_foreign` FOREIGN KEY (`label_id`) REFERENCES `label` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `a_product_a_label_a_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
